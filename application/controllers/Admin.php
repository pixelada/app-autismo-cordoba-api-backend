<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public $ct = 1080;
   
    public function __construct(){
        parent::__construct();
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        $this->lang->load('index');
    }
    
    /*
        Administrator frontpage
    */
    public function index()
	{   
        $this->check_login();
        redirect(site_url('admin/users'));
        //$this->view('dashboard', array());

    }

    /*
        Login page
    */
    public function login()
	{   
        $this->load->library('session');
        
        if($this->user_model->is_logged_in()){
            redirect(site_url('admin')); return true;
        }

       $data = array();
        $_POST = $this->input->post(); 
        if(!empty($_POST)){ 
            $user = (object) $this->user_model->user($_POST['email']);
            
            if(empty($user->email)||md5($_POST['pass'])!=$user->pass||intval($user->role)>0){
               $data['error'] = 'Email o contraseña incorrectos';
            }else{
             $data = $this->user_model->login_user($user->email);
              if($this->user_model->is_logged_in()){
                    redirect(site_url('admin')); return true;
               }
            }
        }

        $this->load->view('panel/theme/login.php',$data);
    }


    /*
        Recover password page
    */
    public function recuperar_contrasena()
	{   
       $_GET = $this->input->get(); 
       
       $data = array();
        if(!empty($_GET['token'])){
            $token =  $_GET['token'];
            $data = (array) json_decode(base64_decode($token)); 
            if(!empty($data['email'])&&!empty($data['pass'])&&!empty($data['time'])){ 
                $user = $this->user_model->user($data['email']);
                if(!empty($user['id'])&&$user['pass']==$data['pass']&&!empty($data['time'])){ 
                    if((time()-intval($data['time']))>86400){ 
                       $data['error'] = 'El enlace para recuperar la contraseña ha caducado.';
                    }else{
                        $this->recuperar_contrasena2($user, $token);
                        return true;
                    }
                }else{
                    redirect(site_url('admin/recuperar-contrasena')); return true;
                }
            }else redirect(site_url('admin/recuperar-contrasena')); return true;
        }

        $_POST = $this->input->post(); 
        if(!empty($_POST)){ 
            $user = (object) $this->user_model->user($_POST['email']);
            
            if(empty($user->email)||intval($user->role)>0){
               $data['error'] = 'No existe ningún usuario administrador con el email introducido';
            }else{
               $data['error'] = '';
              
                $token = base64_encode(json_encode(array('time' => time(), 'email' => $user->email, 'pass' => $user->pass)));
                $link = site_url('admin/recuperar-contrasena/?token='.$token);
                $message = '<p>Hola '.$user->name.',</p>';
                $message .= '<p>Para recuperar tu contraseña haz click en el siguente enlace:</p>';
                $message .= '<p><a href="'.$link.'" style="display: block; text-align: center; width: 200px; font-weight: bold; padding: 5px 20px; text-decoration: none; border-radius: 10px; background: #3BAAE4; color: #fff;" target="_blank">Recuperar contraseña</a></p>';
                $this->send_email($user->email, 'Autismo Córdoba - Recuperar contraseña', $message);
               $data['error'] = 'Te he enviado un email. Revisa tu bandeja de entrada.';
            }
        }

        $this->load->view('panel/theme/recuperar.php',$data);
    }

    /*
        Recover password step 2 page
    */
    private function recuperar_contrasena2($user, $token){
       $data = array('token' => $token);
       $data['user'] = $user;
        if(!empty($_POST['pass'])){
            $this->user_model->change_pass($user['id'], $_POST['pass']); 
           $data['error'] = 'Tu contraseña se ha cambiado correctamente <script> setTimeout(function(){ window.location.href ="'.site_url('admin/login').'" }, 5000); </script>';
           
        }

        $this->load->view('panel/theme/recuperar2.php',$data);
    }


    /*
        This function sends and email
    */
    private function send_email($to, $subject, $message){
       // $to = $item['email'];
       $this->load->library('email');
       //Indicamos el protocolo a utilizar
        $config['protocol'] = 'smtp';
         
       //El servidor de correo que utilizaremos
        $config["smtp_host"] = 'ssl://vps.pixelada.org';
         
       //Nuestro usuario
        $config["smtp_user"] = 'info@pixelada.org';
         
       //Nuestra contraseña
        $config["smtp_pass"] = 'Pixelada2o2o*';   
         
       //El puerto que utilizará el servidor smtp
        $config["smtp_port"] = '465';


        $config['smtp_timeout'] = 5;

     //   $config['newline'] = "\r\n";
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        
       $config['validate'] = true;

        $this->email->initialize($config);

        $this->email->from('info@pixelada.org', 'DentalTEA');

        $this->email->to($to);

        $message = '<p style="margin-bottom:30px;"><img src="'.site_url('assets/images/logo-autismo.png').'" alt="Autismo Córdoba" width="300" style="background:#3BAAE4; padding: 10px;" /></p>'.$message;
        $this->email->subject($subject);
        $this->email->message($message);

        
        if($this->email->send()){
             return true;
        }else{  
            //echo $this->email->print_debugger(); 
            return false;
        }
    }


    /*
        Logout and redirect function
    */
    public function logout(){
        if($this->user_model->is_logged_in())
            $this->user_model->logout($this->user_model->session_id());

        $this->check_login();
    }
   

    /*
        Users section controller
    */
    public function users($accion=null, $id=null)
	{   
        $this->check_login();
       $data = array();
        
        if(empty($accion)){  $this->users_list(); return true;  }

        $user = $this->user_model->user_by_id($id);
       
        if(empty($user)&&($accion=='edit'||$accion=='delete')){
            redirect(site_url('admin/users')); return true;
        }

        switch($accion){
            case 'new': 
                $this->new_user(); break;
            case 'edit': 
                $this->edit_user($user); break;
            case 'delete': 
                $this->user_model->delete_user($user['id']);
                redirect(site_url('admin/users'));
                break;
            default:
                redirect(site_url('admin/users'));
        }
    }


    /*
        Users list page
    */
    private function users_list()
	{
       $data = array('current' => 'users', 'title'=> 'Usuarios');
       $data['users'] = $this->user_model->users(true);
        
        $this->view('users/list',$data);         
    }


    /*
        New user page
    */
    private function new_user()
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            $id = $this->user_model->new_user($_POST);
            redirect(site_url('admin/users'));
            return true;
        }

       $data = array('current' => 'users', 'current_2' => 'new', 'title'=> 'Nuevo usuario');
       $data['users'] = $this->user_model->users(true);
      
        $this->view('users/new',$data);    
    }

    /*
        Edit user page
    */
    private function edit_user($user)
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            $this->user_model->edit_user($user['id'], $_POST);
            redirect(site_url('admin/users'));
            return true;
        }
        
       $data = array('current' => 'users', 'title'=> 'Editar usuario', 'user' => $user);
       $data['users'] = $this->user_model->users(true);
        $this->view('users/edit',$data);         
    }


    /*
        Advices section controller
    */
    public function advices($accion=null, $id=null)
	{   
        $this->check_login();
       $data = array();
        
        if(empty($accion)){  $this->advices_list(); return true;  }

        $advice = $this->advice_model->advice($id);
       
        if(empty($advice)&&($accion=='edit'||$accion=='delete')){
            redirect(site_url('admin/advices')); return true;
        }

        switch($accion){
            case 'new': 
                $this->new_advice(); break;
            case 'edit': 
                $this->edit_advice($advice); break;
            case 'delete': 
                $this->advice_model->delete_advice($advice['id']);
                redirect(site_url('admin/advices'));
                break;
            default:
                redirect(site_url('admin/advices'));
        }
    }


    /*
        Advices list page
    */
    private function advices_list()
	{
       $data = array('current' => 'advices', 'title'=> 'Consejos');
       $data['advices'] = $this->advice_model->advices(true);
        
        $this->view('advices/list',$data);         
    }


    /*
        New advice page
    */
    private function new_advice()
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            $id = $this->advice_model->new_advice($_POST);
            redirect(site_url('admin/advices'));
            return true;
        }

       $data = array('current' => 'advices', 'current_2' => 'new', 'title'=> 'Nuevo usuario');
       $data['advices'] = $this->advice_model->advices(true);
      
        $this->view('advices/new',$data);    
    }

    /*
        Edit advice page
    */
    private function edit_advice($advice)
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            $this->advice_model->edit_advice($advice['id'], $_POST);
            redirect(site_url('admin/advices'));
            return true;
        }
        
       $data = array('current' => 'advices', 'title'=> 'Editar usuario', 'advice' => $advice);
       $data['advices'] = $this->advice_model->advices(true);
        $this->view('advices/edit',$data);         
    }


    /*
        Events section controller
    */
    public function events($accion=null, $id=null)
	{   
        $this->check_login();
       $data = array();
        
        if(empty($accion)){  $this->events_list(); return true;  }

        $event = (array) $this->event_model->event($id);
       
        if(empty($event)&&($accion=='edit'||$accion=='delete')){
            redirect(site_url('admin/events')); return true;
        }

        switch($accion){
            case 'new': 
                $this->new_event(); break;
            case 'edit': 
                $this->edit_event($event); break;
            case 'delete': 
                $this->event_model->delete_event($event['id']);
                redirect(site_url('admin/events'));
                break;
            default:
                redirect(site_url('admin/events'));
        }
    }


    /*
        Events list page
    */
    private function events_list()
	{
       $data = array('current' => 'events', 'title'=> 'Citas');
       $data['events'] = $this->event_model->events(true);
        
        $this->view('events/list',$data);         
    }


    /*
        New event page
    */
    private function new_event()
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            $id = $this->event_model->new_event($_POST);
            redirect(site_url('admin/events/edit/'.$id));
            return true; 
        }

       $data = array('current' => 'events', 'current_2' => 'new', 'title'=> 'Nueva cita');
       $data['events'] = $this->event_model->events(true);
      
        $this->view('events/new',$data);    
    }

    /*
        Edit event page
    */
    private function edit_event($event)
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            if(empty($_POST['sequences'])) $_POST['sequences'] = -1;
            $this->event_model->edit_event($event['id'], $_POST);
            redirect(site_url('admin/events'));
            return true;
        }
        
       $data = array('current' => 'events', 'title'=> 'Editar usuario', 'event_id' => $event['id'], 'item' => (object) $event);
       $data['events'] = $this->event_model->events(true);
        $this->view('events/edit',$data);         
    }


    /*
        Sequences section controller
    */
    public function sequences($accion=null, $id=null)
	{   
        $this->check_login();
       $data = array();
        
        if(empty($accion)){  $this->sequences_list(); return true;  }

        $sequence = (array) $this->sequence_model->sequence($id);
       
        if(empty($sequence)&&($accion=='edit'||$accion=='delete')){
            redirect(site_url('admin/sequences')); return true;
        }

        switch($accion){
            case 'new': 
                $this->new_sequence(); break;
            case 'edit': 
                $this->edit_sequence($sequence); break;
            case 'delete': 
                $this->sequence_model->delete_sequence($sequence['id']);
                redirect(site_url('admin/sequences'));
                break;
            default:
                redirect(site_url('admin/sequences'));
        }
    }


    /*
        Sequences list page
    */
    private function sequences_list()
	{
       $data = array('current' => 'sequences', 'title'=> 'Secuencias');
       $data['sequences'] = $this->sequence_model->sequences(true);
        
        $this->view('sequences/list',$data);         
    }


    /*
        New sequence page
    */
    private function new_sequence()
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){ 
            $id = $this->sequence_model->new_sequence($_POST);
            
            redirect(site_url('admin/sequences/edit/'.$id));
            return true;
        }

       $data = array('current' => 'sequences', 'current_2' => 'new', 'title'=> 'Nueva secuencia');
       $data['sequences'] = $this->sequence_model->sequences(true);
      
        $this->view('sequences/new',$data);    
    }

    /*
        Edit sequence page
    */
    private function edit_sequence($sequence)
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            if(empty($_POST['pictograms'])) $_POST['pictograms'] = -1;
            $this->sequence_model->edit_sequence($sequence['id'], $_POST);
            redirect(site_url('admin/sequences'));
            return true;
        }
        
        $data = array('current' => 'sequences', 'title'=> 'Editar usuario', 'sequence_id' => $sequence['id'], 'item' => (object) $sequence);
        $data['sequences'] = $this->sequence_model->sequences(true);
        $this->view('sequences/edit',$data);         
    }

    /*
        Pictograms section controller
    */
    public function pictograms($accion=null, $id=null)
	{   
        $this->check_login();
       $data = array();
        
        if(empty($accion)){  $this->pictograms_list(); return true;  }

        $pictogram = (array) $this->pictogram_model->pictogram($id);
       
        if(empty($pictogram)&&($accion=='edit'||$accion=='delete')){
            redirect(site_url('admin/pictograms')); return true;
        }

        switch($accion){
            case 'new': 
                $this->new_pictogram(); break;
            case 'edit': 
                $this->edit_pictogram($pictogram); break;
            case 'delete': 
                $this->pictogram_model->delete_pictogram($pictogram['id']);
                redirect(site_url('admin/pictograms'));
                break;
            default:
                redirect(site_url('admin/pictograms'));
        }
    }


    /*
        Pictograms list page
    */
    private function pictograms_list()
	{
       $data = array('current' => 'pictograms', 'title'=> 'Secuencias');
       $data['pictograms'] = $this->pictogram_model->pictograms(true);
        
        $this->view('pictograms/list',$data);         
    }


    /*
        New pictogram page
    */
    private function new_pictogram()
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            $id = $this->pictogram_model->new_pictogram($_POST);
            redirect(site_url('admin/pictograms'));
            return true;
        }

       $data = array('current' => 'pictograms', 'current_2' => 'new', 'title'=> 'Nuevo usuario');
       $data['pictograms'] = $this->pictogram_model->pictograms(true);
      
        $this->view('pictograms/new',$data);    
    }

    /*
        Edit pictogram page
    */
    private function edit_pictogram($pictogram)
	{
        $_POST = $this->input->post(); $message = '';

        if(!empty($_POST)){
            $this->pictogram_model->edit_pictogram($pictogram['id'], $_POST);
            redirect(site_url('admin/pictograms'));
            return true;
        }
        
       $data = array('current' => 'pictograms', 'title'=> 'Editar usuario', 'pictogram_id' => $pictogram['id'], 'item' => (object) $pictogram);
       $this->view('pictograms/edit',$data);         
    }

    /*
        This function checks if the user is logged in
    */
    private function check_login(){
        if(!$this->user_model->is_logged_in()){
            redirect(site_url('admin/login')); 
        }
    }

    /*
        This function calls a view
    */
    private function view($view,$data)
    {
        if(empty($data['current'])) $data['current'] = 'home';
        if(empty($data['current_2'])) $data['current_2'] = '';
       $data['content'] = $this->load->view('panel/'.$view.'.php',$data, true);
        $this->load->view('panel/theme/page.php',$data);
    }
}