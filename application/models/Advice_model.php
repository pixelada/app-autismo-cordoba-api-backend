<?php
/* 
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *  
 *  This file is part of Dentaltea
 *  
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of AppAutismoCórdoba
 *
 *  AppAutismoCórdoba is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AppAutismoCórdoba is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AppAutismoCórdoba.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Advice_model.php */

class Advice_model extends CI_Model {

	/*
       This function returns a list of all advices in the database
    */
    public function advices(){
		$this->db->select('id, title, content, image');
		$q = $this->db->get('advices');
		return $q->result_array();
	}

    /* 
        This function returns the data of a specific advice
    */
	public function advice($id){
		$this->db->select('*');
	   	$q = $this->db->get_where('advices', array('id' => $id));
		$advices = $q->result_array(); 
		if(is_array($advices)&&sizeof($advices)>0)	return $advices[0];
	   	else return null;
	}


    /* 
        This function indicates if a specific advice exists
    */ 
	public function advice_exists($id){
		$this->db->select('*');
	   	$q = $this->db->get_where('advices', array('id' => intval($id)));
	   	$advices = $q->result_array();
	   	if(is_array($advices)&&sizeof($advices)>0)	return true;
	   	else return false;
	}

	/* 
        This function creates a new advice
    */   
    public function new_advice($data){
		$advice = array(); $data = (array) $data;
	
        if(empty($data['content'])) return false;
        if(empty($data['title'])) return false;

        $advice['content'] = $data['content'];
        $advice['title'] = $data['title'];

        $image = $this->upload_file('image');
        if(!empty($image)) $advice['image'] = $image;
        
        $this->db->insert('advices', $advice);

        $insert_id = $this->db->insert_id();

        return $insert_id;
    }


    /* 
        This function edits a specific advice
    */    
    public function edit_advice($id, $data){
        if(!$this->advice_exists($id))    return false;
		$advice_data = $this->advice($id);
		$advice = array();
        
        $data = (array) $data;

		if(empty($data['title'])) return false;
		
		$advice['title'] = $data['title'];
		$advice['content'] = $data['content'];

        $image = $this->upload_file('image');
        
        if(intval($data['image-edit'])==1)
            $advice['image'] = $image;
		
		$adive = array();
        
        $this->db->set($advice);
		$this->db->where('id', intval($id));
        $this->db->update('advices');
        
		return true;
    }


    /* 
        This function uploads a image file
    */
    private function upload_file($image){
        if(empty(basename($_FILES[$image]['name']))) return null;
        $upload_path = BASEPATH.'/../uploads/';

        if(!file_exists($upload_path)) mkdir(($upload_path));
		$upload_url = 'uploads/';
		$info = pathinfo($_FILES[$image]['name']); 
        $file_path = $upload_path . strtoslug($info['filename']).'.'.$info['extension'];

        if(file_exists($file_path)){ 
            $info = pathinfo($file_path);  $i=1;
            while(file_exists($file_path)){
                $file_path = $upload_path.strtoslug($info['filename']).'-'.$i.'.'.$info['extension'];
                $i++;
            }
        }
        if (move_uploaded_file($_FILES[$image]['tmp_name'], $file_path)) 
            return $upload_url.basename($file_path);
        return null;        
	}
	
	/* 
        This function deletes a specific advice
    */
    public function delete_advice($id){
        $this->db->delete('advices', array('id' => intval($id)));
    }
}