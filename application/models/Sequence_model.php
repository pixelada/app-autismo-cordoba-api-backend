<?php
/* 
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *  
 *  This file is part of Dentaltea
 *  
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of AppAutismoCórdoba
 *
 *  AppAutismoCórdoba is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AppAutismoCórdoba is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AppAutismoCórdoba.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Sequence_model.php */


class Sequence_model extends CI_Model {
    /*
       This function returns a list of all sequences in the database
    */
    public function sequences(){
        $this->db->select('*');
		$q = $this->db->get('sequences');
		return $q->result_array();
    }
    
    
    /* 
        This function returns the data of a specific sequence
    */
    public function sequence($id){
        $this->db->select('*');
	   	$q = $this->db->get_where('sequences', array('id' => $id));
		$sequence = $q->row(); 
        if(!empty($sequence->id))
            $sequence->pictograms = $this->sequence_pictograms($sequence->id);  
            
        $this->db->select('*');
        $q = $this->db->get_where('pictograms', array('id' => $sequence->thumbnail_id));
        $pictogram = $q->row(); 
        
        if(!empty($pictogram->image))
            $sequence->thumbnail = $pictogram->image; 

        return $sequence;
    }

    /*
       This function returns the url of the thumbnail
	*/
	public function sequence_thumbnail($id){
        $sequence = $this->sequence($id);
        if(empty($sequence->thumbnail_id))  return '';

        $this->db->select('*');
	   	$q = $this->db->get_where('pictograms', array('id' => $sequence->thumbnail_id));
        $pictogram = $q->row(); 
        
        if(!empty($pictogram->image))   return site_url($pictogram->image);
        else return '';
	}

    /* 
        This function indicates if a specific sequence exists
    */
    public function sequence_exists($id){
		$this->db->select('*');
	   	$q = $this->db->get_where('sequences', array('id' => $id));
        $sequence = $q->row(); 
		if(!empty($sequence->id))	return true;
	   	else return false;
    }
    
    /* 
        This function returns the pictograms of a specific sequence
    */
    public function sequence_pictograms($sequence_id){
        if(!$this->sequence_exists($sequence_id))    return false;
        
        $sql = 'SELECT p.*, sp.sequence_id, sp.insert_order FROM sequence_pictograms sp, pictograms p WHERE p.id=sp.pictogram_id AND sp.sequence_id='.intval($sequence_id).' ORDER BY sp.insert_order ASC;';
		$q = $this->db->query($sql);
		$resultados = $q->result_array();
        
        return $resultados;
    }

    /* 
        This function indicates if a pictogram is part of a specific sequence
    */
    public function sequence_has_pictogram($sequence_id, $pictogram_id){
        $sql = 'SELECT * FROM sequence_pictograms WHERE sequence_id='.intval($sequence_id).' AND pictogram_id='.intval($pictogram_id).';';
		$q = $this->db->query($sql);
		$resultados = $q->result_array();
        if(!empty($resultados)) return true;
        else return false;
    }

    /* 
        This function returns a list of all the sequences that a given user can view in the app
    */
    public function sequences_user($user_id){
        $sql = 'SELECT * FROM sequences WHERE author_id IS NULL OR author_id='.intval($user_id).'; ';
        $q = $this->db->query($sql);
		return $q->result_array();
	}

    
    /* 
        This function creates a new sequence
    */
    public function new_sequence($data){
        $sequence = array(); $data = (array) $data;
	
        if(empty($data['title'])) return false;

        if(!empty($data['user_id'])&&$this->user_exists_id($data['user_id'])) $sequence['author_id'] = $data['user_id'];

        $sequence['title'] = $data['title'];

        if(!empty($data['description']))
            $sequence['description'] = $data['description'];

        $this->db->insert('sequences', $sequence);
        $insert_id = $this->db->insert_id();
        
        if(!empty($data['pictograms'])){
            $pictograms = explode(',', str_replace(' ','',$data['pictograms']));
            $this->edit_sequence_pictograms($id, $pictograms);
        }
        
        return $insert_id;
    }


    /* 
        This function edits a specific sequence
    */
    public function edit_sequence($id, $data){
        if(!$this->sequence_exists($id))    return false;
        $sequence = (array) $this->sequence($id);
        
        $data = (array) $data;
		if(!empty($data['user_id'])&&$this->user_exists_id($data['user_id'])) $sequence['author_id'] = $data['user_id'];
        
        $sequence = array();
		$sequence['title'] = $data['title'];

        if(!empty($data['description']))
            $sequence['description'] = $data['description'];
        
        $this->db->set($sequence);
		$this->db->where('id', intval($id));
        $this->db->update('sequences');
        

        if($data['pictograms']==-1){
            $this->clear_sequence_pictograms($id);
        }else{
            if(!empty($data['pictograms'])){
                $pictograms = explode(',', str_replace(' ','',$data['pictograms']));
                $this->edit_sequence_pictograms($id, $pictograms);
            }
        }

		return true;
    }


    /* 
        This function edits the pictograms of a specific sequence
    */
    public function edit_sequence_pictograms($id, $pictograms){
        $this->clear_sequence_pictograms($id);

        if(empty($pictograms)||!is_array($pictograms)) return false;

        foreach($pictograms as $pictogram)
            if($this->pictogram_exists($pictogram))    
                $this->add_sequence_pictogram($id, $pictogram);
    }

    /* 
        This function indicates if a specific pictogram exists
    */
	public function pictogram_exists($id){
		$this->db->select('*');
	   	$q = $this->db->get_where('pictograms', array('id' => $id));
        $pictogram = $q->row(); 
		if(!empty($pictogram->id))	return true;
	   	else return false;
	}

    /* 
        This function adds a pictogram to a sequence
    */
    public function add_sequence_pictogram($sequence_id, $pictogram_id){
        if(!$this->sequence_exists($sequence_id))    return false;
        
        //if($this->sequence_has_pictogram($sequence_id, $pictogram_id)) return false;
        
        $this->db->insert('sequence_pictograms', array('sequence_id' => intval($sequence_id), 'pictogram_id' => intval($pictogram_id), 'insert_order' => $this->sequence_pictogram_next($sequence_id)));

        return true;
    }

    /* 
        This function clears all the pictograms of a specific sequence
    */
    public function clear_sequence_pictograms($sequence_id){
        if(!$this->sequence_exists($sequence_id))    return false;
        $this->db->delete('sequence_pictograms', array('sequence_id' => intval($sequence_id)));
    }

    
    /* 
        This function returns the following pictogram number in the established order
    */
    public function sequence_pictogram_next($sequence_id){
        if(!$this->sequence_exists($sequence_id))    return false;
        
        $resultados = $this->sequence_pictograms($sequence_id);
        
        if(empty($resultados)||!is_array($resultados)) return 1;
        else{ 
            if(empty($resultados[sizeof($resultados)-1]['insert_order'])) return 1;
            $order = intval($resultados[sizeof($resultados)-1]['insert_order']) + 1;
            return $order;
        }
    }

    /* 
        This function deletes a specific sequence
    */
    public function delete_sequence($id){
        $this->db->delete('sequences', array('id' => intval($id)));
    }
    
    /* 
        This function deletes a specific sequence if it belongs to a given user
    */
    public function delete_sequence_user($id, $user_id){
        $sequence = $this->sequence($id); 
       
        if(empty($sequence->id)||intval($user_id)!=intval($sequence->author_id))
            return false;
		$this->db->delete('sequences', array('id' => intval($id)));
    }

     /* 
        This function indicates if a user exists by passing a specific parameter
    */
    public function user_exists_by($array){
		$this->db->select('*');
	   	$q = $this->db->get_where('users', $array);
	   	$users = $q->result_array();
	   	if(is_array($users)&&sizeof($users)>0)	return true;
	   	else return false;
	}


    /* 
        This function indicates if a user exists by passing the id
    */
    public function user_exists_id($id){
		return $this->user_exists_by(array('id' => intval($id)));
    }
}