<?php
/* 
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *  
 *  This file is part of Dentaltea
 *  
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of AppAutismoCórdoba
 *
 *  AppAutismoCórdoba is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AppAutismoCórdoba is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AppAutismoCórdoba.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Event_model.php */

class Event_model extends CI_Model {
    
    /*
       This function returns a list of all events in the database
    */
    public function events(){
        $this->db->select('*');
        $q = $this->db->get('events');
        return $q->result_array();
    }

    
    /* 
        This function returns the data of a specific event
    */    
    public function event($id){
		
        $this->db->select('*');
	   	$q = $this->db->get_where('events', array('id' => $id));
		$event = $q->row(); 
        if(!empty($event))
            $event->sequences = $this->event_sequences($id);
        return $event;
    }


    /* 
        This function indicates if a specific event exists
    */    
    public function event_exists($id){
		$this->db->select('*');
	   	$q = $this->db->get_where('events', array('id' => $id));
        $event = $q->row(); 
		if(!empty($event->id))	return true;
	   	else return false;
    }


    /* 
        This function returns a list of all the events that a given user can view in the app
    */    
    public function events_user($user_id){
        $sql = 'SELECT * FROM events WHERE user_id='.intval($user_id).'; ';
        $q = $this->db->query($sql);
        $events = $q->result_array();
        
        return $events;
    }

    
    /* 
        This function returns the sequences of a specific event
    */
    public function event_sequences($event_id){
        if(!$this->event_exists($event_id))    return false;
        
        $sql = 'SELECT s.*, es.event_id, es.insert_order FROM event_sequences es, sequences s WHERE s.id=es.sequence_id AND es.event_id='.intval($event_id).' ORDER BY es.insert_order ASC;';
		$q = $this->db->query($sql);
        $resultados = $q->result_array();
        
        for($i=0;$i<sizeof($resultados);$i++){
            $resultados[$i] = (object) $resultados[$i];
            $sql = 'SELECT p.*, sp.insert_order FROM sequence_pictograms sp, pictograms p WHERE p.id=sp.pictogram_id AND sp.sequence_id='.intval($resultados[$i]->id).' ORDER BY sp.insert_order ASC;';
            $q = $this->db->query($sql);
            $resultados[$i]->pictogramas = $q->result_array();
        }
        
        return $resultados;
    }


    /* 
        This function indicates if a sequence is part of a specific event
    */
    public function event_has_sequence($event_id, $sequence_id){
        $sql = 'SELECT * FROM event_sequences WHERE event_id='.intval($event_id).' AND sequence_id='.intval($sequence_id).';';
		$q = $this->db->query($sql);
		$resultados = $q->result_array();
        if(!empty($resultados)) return true;
        else return false;
    }


    /* 
        This function indicates if a specific sequence exists
    */
    public function sequence_exists($id){
		$this->db->select('*');
	   	$q = $this->db->get_where('sequences', array('id' => $id));
        $sequence = $q->row(); 
		if(!empty($sequence->id))	return true;
	   	else return false;
    }

    /* 
        This function indicates if a user exists by passing the id
    */
    public function user_exists_id($id){
		return $this->user_exists_by(array('id' => intval($id)));
    }

    
    /* 
        This function indicates if a user exists by passing a specific parameter
    */
    public function user_exists_by($array){
		$this->db->select('*');
	   	$q = $this->db->get_where('users', $array);
	   	$users = $q->result_array();
	   	if(is_array($users)&&sizeof($users)>0)	return true;
	   	else return false;
	}


    /* 
        This function creates a new event
    */   
    public function new_event($data){
		$event = array(); $data = (array) $data;
	
        if(empty($data['datetime'])||empty($data['user_id'])) return false;
        
        $event['datetime'] = $data['datetime'];
        $event['user_id'] = $data['user_id'];
        if(!$this->user_exists_id($data['user_id']))  return false;
        
        if(!empty($data['description']))
            $event['description'] = $data['description'];
        
        $this->db->insert('events', $event);

        $insert_id = $this->db->insert_id();

        return $insert_id;
    }


    /* 
        This function edits a specific event
    */    
    public function edit_event($id, $data){
        if(!$this->event_exists($id))    return false;
        $event = $this->event($id);
        
        $data = (array) $data;
		if(empty($data['user_id'])||intval($data['user_id'])!=intval($event->user_id))
            return false;
        
        $event = array();
		$event['datetime'] = $data['datetime'];
        if(!empty($data['description']))
            $event['description'] = $data['description'];
        
        $this->db->set($event);
		$this->db->where('id', intval($id));
        $this->db->update('events');
        
        if($data['sequences']==-1){
            $this->clear_event_sequences($id);
        }else{
            if(!empty($data['sequences'])){
                $sequences = explode(',', str_replace(' ','',$data['sequences']));
                $this->edit_event_sequences($id, $sequences);
            }
        }

		return true;
    }


    /* 
        This function edits the sequences of a specific event
    */
    public function edit_event_sequences($event_id, $sequences){
        $this->clear_event_sequences($event_id);

        if(empty($sequences)||!is_array($sequences)) return false;
        
        foreach($sequences as $sequence)
            $this->add_event_sequence($event_id, $sequence);
    }


    /* 
        This function adds a pictogram to a sequence
    */
    public function add_event_sequence($event_id, $sequence_id){
        if(!$this->event_exists($event_id))    return false;
        
  //    if($this->event_has_sequence($event_id, $sequence_id)) return false;
        if(empty($sequence_id)) return false;

        $this->db->insert('event_sequences', array('event_id' => intval($event_id), 'sequence_id' => intval($sequence_id), 'insert_order' => $this->event_sequence_next($event_id)));

        return true;
    }

    
    /* 
        This function clears all the sequences of a specific event
    */
    public function clear_event_sequences($event_id){
        if(!$this->event_exists($event_id))    return false;
        $this->db->delete('event_sequences', array('event_id' => intval($event_id)));
    }


    /* 
        This function returns the following sequence number in the established order
    */
    public function event_sequence_next($event_id){
        if(!$this->event_exists($event_id))    return false;
        
        $resultados = $this->event_sequences($event_id);
        
        if(empty($resultados)||!is_array($resultados)) return 1;
        else{ 
            if(empty($resultados[sizeof($resultados)-1]->insert_order)) return 1;
            $order = intval($resultados[sizeof($resultados)-1]->insert_order) + 1;
            return $order;
        }
    }

    
    /* 
        This function deletes a specific event
    */
    public function delete_event($id){
        $this->clear_event_sequences($id);
		$this->db->delete('events', array('id' => intval($id)));
    }

    
    /* 
        This function deletes a specific event if it belongs to a given user
    */
    public function delete_event_user($id, $user_id){
        $event = $this->event($id);
        if(empty($event->id)||intval($user_id)!=intval($event->user_id))
            return false;
		$this->db->delete('events', array('id' => intval($id)));
    }
    
}