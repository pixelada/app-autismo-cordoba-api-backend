<?php
/* 
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *  
 *  This file is part of Dentaltea
 *  
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of AppAutismoCórdoba
 *
 *  AppAutismoCórdoba is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AppAutismoCórdoba is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AppAutismoCórdoba.  If not, see <https://www.gnu.org/licenses/>.
 */


/* Pictogram_model.php */

class Pictogram_model extends CI_Model {
    
    /*
       This function returns a list of all pictograms in the database
	*/
	public function pictograms(){
        $this->db->select('*');
		$q = $this->db->get('pictograms');
		return $q->result_array();
	}

    
    /* 
        This function returns the data of a specific pictogram
    */
	public function pictogram($id){
        $this->db->select('*');
	   	$q = $this->db->get_where('pictograms', array('id' => $id));
		$pictogram = $q->row(); 
        return $pictogram;
	}

    /* 
        This function indicates if a specific pictogram exists
    */
	public function pictogram_exists($id){
		$this->db->select('*');
	   	$q = $this->db->get_where('pictograms', array('id' => $id));
        $pictogram = $q->row(); 
		if(!empty($pictogram->id))	return true;
	   	else return false;
    }
    

    /* 
        This function creates a new pictogram
    */
    public function new_pictogram($data){
        $pictogram = array(); $data = (array) $data;
	
        if(!empty($data['description']))
            $pictogram['description'] = $data['description'];

        $image = $this->upload_file('image');
        if(!empty($image)) $pictogram['image'] = $image;
        else return false;

        $this->db->insert('pictograms', $pictogram);
        $insert_id = $this->db->insert_id();
        
        return $insert_id;
    }


    /* 
        This function edits a specific pictogram
    */
    public function edit_pictogram($id, $data){
        if(!$this->pictogram_exists($id))    return false;
        $pictogram = $this->pictogram($id);
        
        $data = (array) $data;
	    
        $pictogram = array();
        
        if(!empty($data['description']))
            $pictogram['description'] = $data['description'];
        
        $image = $this->upload_file('image');
        if(!empty($image)) $pictogram['image'] = $image;

        $this->db->set($pictogram);
		$this->db->where('id', intval($id));
        $this->db->update('pictograms');

		return true;
    }


    /* 
        This function deletes a specific pictogram
    */    
    public function delete_pictogram($id){
        $this->db->delete('pictograms', array('id' => intval($id)));
    }
    
    /* 
        This function uploads a image file
    */
    private function upload_file($image){
        if(empty(basename($_FILES[$image]['name']))) return null;
        $upload_path = BASEPATH.'/../uploads/';

        if(!file_exists($upload_path)) mkdir(($upload_path));
		$upload_url = 'uploads/';
		$info = pathinfo($_FILES[$image]['name']); 
        $file_path = $upload_path . strtoslug($info['filename']).'.'.$info['extension'];

        if(file_exists($file_path)){ 
            $info = pathinfo($file_path);  $i=1;
            while(file_exists($file_path)){
                $file_path = $upload_path.strtoslug($info['filename']).'-'.$i.'.'.$info['extension'];
                $i++;
            }
        }
        if (move_uploaded_file($_FILES[$image]['tmp_name'], $file_path)) 
            return $upload_url.basename($file_path);
        return null;        
    }
}