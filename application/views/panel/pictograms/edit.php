<h1 class="display-5">Editar pictograma</h1>
<hr />
<a href="<?php echo site_url('admin/pictograms'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    <i class="feather-arrow-left"></i> Volver al listado
</a>
<hr />
<div id="datos-item">
    <form id="form-item" method="post" enctype="multipart/form-data" action=""> 
        <div class="row"> 
            <div class="col-md-6">    
                <div id="field-image" class="form-group">
                    <label>Imagen *</label> 
                    <input type="file" id="image" name="image" class="dropify" data-default-file="<?php if(!empty($item->image)): echo site_url($item->image); endif; ?>" data-allowed-file-extensions='["jpg", "jpeg", "png", "pneg", "gif", "bmp", "tif", "tiff"]' accept=".jpg,.jpeg,.png,.pneg,.gif,.bmp,.tif,.tiff" data-max-file-size="4M" />
                </div>
                   
            </div>
            <div class="col-md-6">  
                <div id="field-description" class="form-group">
                    <label for="description">Descripción</label>
                    <textarea class="form-control" id="description" name="description" rows="2" maxlength="280"><?php if(!empty($item->description)) echo $item->description; ?></textarea>
                </div>
            </div>
        </div>
        <hr />
        <p>* Campos obligatorios</p>
        <div id="msj" class="card-title"></div>
        
        <button id="button-save" type="button" class="btn btn-primary mb-2">Guardar pictograma</button>
    </form>
</div>


<script>
jQuery(function($) {
    $('#button-save').click(function(e){
        $('#msj').html('');
        if($('#field-image .dropify-preview .dropify-render').html()==''){
            $('#msj').html('Debes rellenar los campos obligatorios');
            return false;
        }

        $('#form-item .form-group .required').each(function(){
            if($(this).val()==''){
                $('#msj').html('Debes rellenar los campos obligatorios');
                return false;
            }
        });

        if($('#msj').html()!=''){ 
            e.preventDefault();
            return false;
        } 
        $('#form-item').submit();
        return true;
    });

    
});

    
</script>

                                