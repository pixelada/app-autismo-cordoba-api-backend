<h1 class="display-5">Pictogramas</h1>
<hr />
<a id="boton-nuevo" href="<?php echo site_url('admin/pictograms/new'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    Nuevo pictograma
</a>

<div id="lista-pictograms" class="row">
<div class="col-md-6">


    <table id="tabla-pictograms" class="table dt-responsive nowrap">
        <thead>
            <tr>
                <th>ID</th>
                <th>Descripción</th>
                <th>Miniatura</th>
                <th width="30"></th>
            </tr>
        </thead>
    
    
        <tbody>
        <?php $i=0; foreach($pictograms as $pictogram): $i++; ?>
            <tr>
                <td><?php echo $pictogram['id']; ?></td>
                <td><a href="<?php echo site_url('admin/pictograms/edit/'.$pictogram['id']); ?>"><strong><?php echo $pictogram['description']; ?></strong></a></td>
                <td><?php if(!empty($pictogram['image'])): ?><a href="<?php echo site_url('admin/pictograms/edit/'.$pictogram['id']); ?>"><img height="100" src="<?php echo site_url($pictogram['image']); ?>" alt="<?php echo $pictogram['description']; ?>" /></a><?php endif; ?></td>
                 <td>
                    <a class="link-editar" href="<?php echo site_url('admin/pictograms/edit/'.$pictogram['id']); ?>"><i class="fas fa-edit"></i></a>
                    <a class="link-eliminar" onclick="return confirm('¿Deseas eliminar esta pictograma?')" href="<?php echo site_url('admin/pictograms/delete/'.$pictogram['id']); ?>"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
        <?php if($i==0) echo '<tr><td colspan="6">No se encontraron resultados</td></tr>'; ?>
        </tbody>
    </table>
</div>
</div>

<script>
 <?php if($i>0): ?>
    $('#tabla-pictograms').DataTable({
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        },
        "scrollX": true,
        "autoWidth": false,
        "pageLength" : 25,
        "drawCallback": function () {
            $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
    });
<?php endif; ?>
</script>