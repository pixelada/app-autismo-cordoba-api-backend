<h1 class="display-5">Consejos</h1>
<hr />
<a id="boton-nuevo" href="<?php echo site_url('admin/advices/new'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    Nuevo consejo
</a>

<div id="lista-advices" class="row">
<div class="col-md-6">



    <table id="tabla-advices" class="table dt-responsive nowrap">
        <thead>
            <tr>
                <th>ID</th>
                <th>Título</th>
                <th width="30"></th>
            </tr>
        </thead>
    
    
        <tbody>
        <?php $i=0; foreach($advices as $advice): $i++; ?>
            <tr>
                <td><?php echo $advice['id']; ?></td>
                <td><a href="<?php echo site_url('admin/advices/edit/'.$advice['id']); ?>"><strong><?php echo $advice['title']; ?></strong></a></td>
                <td>
                    <a class="link-editar" href="<?php echo site_url('admin/advices/edit/'.$advice['id']); ?>"><i class="fas fa-edit"></i></a>
                    <a class="link-eliminar" onclick="return confirm('¿Deseas eliminar este consejo?')" href="<?php echo site_url('admin/advices/delete/'.$advice['id']); ?>"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        <?php if($i==0) echo '<tr><td colspan="3">No se encontraron resultados</td></tr>'; ?>
        </tbody>
    </table>
</div>
</div>

<script>
<?php if($i>0): ?>
    $('#tabla-advices').DataTable({
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        },
        "order" : [[1, 'desc']],
        "pageLength" : 25,
        "scrollX": true,
        "autoWidth": false,
        "drawCallback": function () {
            $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
    });
<?php endif; ?>
    
</script>