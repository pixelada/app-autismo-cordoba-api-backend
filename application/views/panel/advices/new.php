<h1 class="display-5">Nuevo consejo</h1>
<hr />
<a href="<?php echo site_url('admin/advices'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    <i class="feather-arrow-left"></i> Volver al listado
</a>
<hr />
<div id="datos-item">
    <form id="form-item" method="post" enctype="multipart/form-data" action=""> 
        <div class="row"> 
            <div class="col-md-12">
                <div id="field-title" class="form-group">
                    <label for="simpleinput">Título *</label>
                    <input type="text" id="title" name="title" class="form-control required" value="" placeholder=""/>
                </div>    
                <div id="field-content" class="form-group">
                    <label for="content">Contenido</label>
                    <input type="hidden" id="content" name="content" value="" />
                    <div id="content-editor" name="content" style="height: 400px;">
                        
                    </div> <!-- end Snow-editor-->
                </div>
                <div id="field-image" class="form-group">
                    <label>Imagen</label> 
                    <input type="file" id="image" name="image" class="dropify" data-default-file="" data-allowed-file-extensions='["jpg", "jpeg", "png", "pneg", "gif", "bmp", "tif", "tiff"]' accept=".jpg,.jpeg,.png,.pneg,.gif,.bmp,.tif,.tiff" data-max-file-size="4M" />
                </div>
            </div>
        </div>
        <hr />
        <p>* Campos obligatorios</p>
        <div id="msj" class="card-title"></div>
        
        <button id="button-save" type="button" class="btn btn-primary mb-2">Crear consejo</button>
    </form>
</div>


<script>
jQuery(function($) {

    var quill = new Quill('#content-editor', {
        theme: 'snow',
        modules: {
            'toolbar': [ ['bold', 'italic', 'underline', 'strike'], [{ 'color': [] }, { 'background': [] }], [{ 'script': 'super' }, { 'script': 'sub' }], [{ 'header': [false, 1, 2, 3, 4, 5, 6] }, 'blockquote', 'code-block'], [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }], ['direction', { 'align': [] }], ['link'], ['clean']]
        },
    });

    

    $('#button-save').click(function(e){
        $('#msj').html('');
        var myEditor = document.querySelector('#content-editor');
        var html = myEditor.children[0].innerHTML;
        $('#content').val(html);

        $('#form-item .form-group .required').each(function(){
            if($(this).val()==''){
                $('#msj').html('Debes rellenar los campos obligatorios');
                return false;
            }
        });

        if($('#msj').html()!=''){ 
            e.pradviceDefault();
            return false;
        } 
        $('#form-item').submit();
        return true;
    });
    
  
  
   
    
});

    
</script>

                                