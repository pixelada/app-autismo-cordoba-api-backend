<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

<div data-simplebar class="h-100">

    <!-- LOGO -->
    <div class="navbar-brand-box" style="padding:0 0 20px;">
        <a href="<?php echo site_url('admin'); ?>" class="logo">
            <span>
                <img id="logo-menu" src="<?php echo site_url('assets/images/logo-autismo.png'); ?>" alt="" style="width:100%;height: auto;">
            </span>
            <i><img src="<?php echo site_url('assets/images/logo-small.png'); ?>" alt="" height="24"></i>
        </a>
    </div>

    <!--- Sidemenu -->
    <div id="sidebar-menu">
        <!-- Left Menu Start -->
        <ul class="metismenu list-unstyled" id="side-menu">
            <li class="menu-title">Menu</li>
            <!--            <li class="<?php if($current=='home'){ echo 'mm-active'; } ?>">
                <a href="<?php echo site_url('admin'); ?>" class="<?php if($current=='home'){ echo 'active'; } ?>"><i class="feather-home"></i><span>Panel</span></a>
            </li>
-->
            <li class="<?php if($current=='pictograms'){ echo 'mm-active'; } ?>">
                <a href="<?php echo site_url('admin/pictograms'); ?>" class="<?php if($current=='pictograms'){ echo 'active'; } ?>"><i class="feather-image"></i><span>Pictogramas</span></a>                
            </li>
            <li class="<?php if($current=='sequences'){ echo 'mm-active'; } ?>">
                <a href="<?php echo site_url('admin/sequences'); ?>" class="<?php if($current=='sequences'){ echo 'active'; } ?>"><i class="feather-grid"></i><span>Secuencias</span></a>                
            </li>
            <li class="<?php if($current=='events'){ echo 'mm-active'; } ?>">
                <a href="<?php echo site_url('admin/events'); ?>" class="<?php if($current=='events'){ echo 'active'; } ?>"><i class="feather-calendar"></i><span>Citas</span></a>                
            </li>
            <li class="<?php if($current=='advices'){ echo 'mm-active'; } ?>">
                <a href="<?php echo site_url('admin/advices'); ?>" class="<?php if($current=='advices'){ echo 'active'; } ?>"><i class="feather-book-open"></i><span>Consejos</span></a>
            </li>
            <li class="<?php if($current=='users'){ echo 'mm-active'; } ?>">
                <a href="<?php echo site_url('admin/users'); ?>" class="<?php if($current=='users'){ echo 'active'; } ?>"><i class="feather-users"></i><span>Usuarios</span></a>
            </li>
        </ul>
    </div>
    <!-- Sidebar -->
</div>
</div>
<!-- Left Sidebar End -->