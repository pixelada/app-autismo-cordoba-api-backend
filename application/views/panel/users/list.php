<h1 class="display-5">Usuarios</h1>
<hr />
<a id="boton-nuevo" href="<?php echo site_url('admin/users/new'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    Nuevo usuario
</a>
<div id="lista-users" class="row">
<div class="col-md-6">
    <table id="tabla-users" class="table dt-responsive nowrap">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Nivel</th>
                <th>Activo</th>
                <th></th>
            </tr>
        </thead>
    
    
        <tbody>
        <?php $i=0; foreach($users as $user): $i++; ?>
            <tr>
                <td><?php echo $user['id']; ?></td>
                <td><a href="<?php echo site_url('admin/users/edit/'.$user['id']); ?>"><strong><?php echo $user['name']; ?></strong></a></td>
                <td><?php echo $user['email']; ?></td>
                <td><?php echo ($user['role']==0)? '<strong>Administrador</strong>' : 'Usuario App';  ?></td>
                <td><?php echo (empty($user['active']))? 'Inactivo' : 'Activo';  ?></td>
                <td>
                    <a class="link-editar" href="<?php echo site_url('admin/users/edit/'.$user['id']); ?>"><i class="fas fa-edit"></i></a>
                    <a class="link-eliminar" onclick="return confirm('¿Deseas eliminar este usuario?')" href="<?php echo site_url('admin/users/delete/'.$user['id']); ?>"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>

<script>
<?php if($i>0): ?>
    $('#tabla-users').DataTable({
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        },
        "scrollX": true,
        "autoWidth": false,
        "pageLength" : 25,
        "drawCallback": function () {
            $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
    });
<?php endif; ?>
</script>