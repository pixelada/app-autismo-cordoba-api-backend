<h1 class="display-5">Editar ususario</h1>
<hr />
<a href="<?php echo site_url('admin/users'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    <i class="feather-arrow-left"></i> Volver al listado
</a>
<hr />
<div id="datos-user">
    <form id="form-user" method="post" enctype="multipart/form-data" action=""> 
        <div class="row"> 
            <div class="col-md-6">    
                <div id="field-active" class="form-group">
                    <label>Activo</label><br />
                    <input type="checkbox" <?php if($user['active']==1): echo 'checked'; endif; ?> id="active" name="active" data-toggle="switchery" data-color="#3BAAE4"/>
                </div>
                <div class="form-group">
                    <h6 class="mt-4">Tipo de usuario</h6>
                    <div class="mt-3">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="tipo-app_user" name="role" class="custom-control-input" value="1" <?php if($user['role']==1): echo 'checked'; endif; ?> />
                            <label class="custom-control-label" for="tipo-app_user">Usuario App</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="tipo-administrador" name="role" class="custom-control-input" value="0"  <?php if($user['role']==0): echo 'checked'; endif; ?> />
                            <label class="custom-control-label" for="tipo-administrador" >Administrador</label>
                        </div>
                    </div>
                </div>
                <div id="field-solucion" class="form-group">
                    <label for="simpleinput">Nombre *</label>
                    <input type="text" id="name" name="name" class="form-control required" value="<?php echo $user['name']; ?>" placeholder=""/>
                </div>
               
                <div class="form-group">
                    <label for="email">Email *</label>
                    <input type="email" class="form-control required" name="email" id="email" placeholder="" value="<?php echo $user['email']; ?>">
                </div>
                <div class="form-group">
                    <label for="pass">Contraseña</label>
                    <input type="password" id="pass" name="pass" class="form-control" value="">
                </div>
                <div class="form-group">
                    <label for="pass2">Repetir contraseña</label>
                    <input type="password" id="pass2" class="form-control" value="">
                </div>
                <p>* Campos obligatorios</p>
                <div id="msj" class="card-title"></div>
            </div>
            
        </div>
        
        <button id="boton-guardar" type="button" class="btn btn-primary mb-2">Guardar</button>
    </form>
</div>


<script>
jQuery(function($) {
    $('#boton-guardar').click(function(e){ 
        $('#msj').html('');
       
        $('#form-user .form-group .required').each(function(){
            if($(this).val()==''){
                $('#msj').html('Debes rellenar los campos obligatorios');
                return false;
            }
        });

        if(!validar_email($('#email').val())){
            $('#msj').html('El email introducido no es correcto');
            return false;
        }

        var emails = [<?php foreach($users as $u): if($u['id']!=$user['id']): echo '"'.$u['email'].'",'; endif; endforeach; ?>];

        for(var i=0;i<emails.length;i++){
            if($('#email').val()==emails[i]){
                $('#msj').html('El email introducido ya fue utilizado por otro user');
                return false;
            }
        }

        if($('#pass').val()!=''||$('#pass2').val()!=''){
            if($('#pass').val().length<6){
                $('#msj').html('La contraseña debe tener al menos 6 caracteres.');
                return false;
            }

            if($('#pass').val()!=$('#pass2').val()){
                $('#msj').html('Las contraseñas no coinciden');
                return false;
            }
        }

        if($('#msj').html()!=''){ 
            e.preventDefault();
            return false;
        } 
        $('#form-user').submit();
        return true;
    });
   
});

</script>