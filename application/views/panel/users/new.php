<h1 class="display-5">Nuevo usuario</h1>
<hr />
<a href="<?php echo site_url('admin/users'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    <i class="feather-arrow-left"></i> Volver al listado
</a>
<hr />
<div id="datos-usuario">
    <form id="form-usuario" method="post" enctype="multipart/form-data" action=""> 
        <div class="row"> 
            <div class="col-md-6">    
                <div id="field-active" class="form-group">
                    <label>Activo</label><br />
                    <input type="checkbox" checked id="active" name="active" data-toggle="switchery" data-color="#3BAAE4"/>
                </div>
                <div class="form-group">
                    <h6 class="mt-4">Tipo de usuario</h6>
                    <div class="mt-3">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="tipo-app_user" name="role" class="custom-control-input" value="1" checked />
                            <label class="custom-control-label" for="tipo-app_user">Usuario App</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="tipo-administrador" name="role" class="custom-control-input" value="0"  />
                            <label class="custom-control-label" for="tipo-administrador" >Administrador</label>
                        </div>
                    </div>
                </div>
                <div id="field-solucion" class="form-group">
                    <label for="simpleinput">Nombre *</label>
                    <input type="text" id="name" name="name" class="form-control required" value="" placeholder=""/>
                </div>
                <div class="form-group">
                    <label for="email">Email *</label>
                    <input type="email" class="form-control required" name="email" id="email" placeholder="">
                </div>
                <div class="form-group">
                    <label for="pass">Contraseña *</label>
                    <input type="password" id="pass" name="pass" class="form-control required" value="">
                </div>
                <div class="form-group">
                    <label for="pass2">Repetir contraseña *</label>
                    <input type="password" id="pass2" class="form-control required" value="">
                </div>
                <p>* Campos obligatorios</p>
                <div id="msj" class="card-title"></div>
            </div>
            
        </div>
        
        <button id="boton-guardar" type="button" class="btn btn-primary mb-2">Crear usuario</button>
    </form>
</div>


<script>
jQuery(function($) {
    $('#boton-guardar').click(function(e){ 
        $('#msj').html('');
       
        $('#form-usuario .form-group .required').each(function(){
            if($(this).val()==''){
                $('#msj').html('Debes rellenar los campos obligatorios');
                return false;
            }
        });

        if(!validar_email($('#email').val())){
            $('#msj').html('El email introducido no es correcto');
            return false;
        }

        var emails = [<?php foreach($usuarios as $u): echo '"'.$u['email'].'",'; endforeach?>];

        for(var i=0;i<emails.length;i++){
            if($('#email').val()==emails[i]){
                $('#msj').html('El email introducido ya fue utilizado por otro usuario');
                return false;
            }
        }

        if($('#pass').val().length<6){
            $('#msj').html('La contraseña debe tener al menos 6 caracteres.');
            return false;
        }

        if($('#pass').val()!=$('#pass2').val()){
            $('#msj').html('Las contraseñas no coinciden');
            return false;
        }

        if($('#msj').html()!=''){ 
            e.preventDefault();
            return false;
        } 
        $('#form-usuario').submit();
        return true;
    });
   
});

</script>

                                