<h1 class="display-5">Configuración</h1>
<hr />

<div id="datos-form">
<form id="form-datos" method="post" enctype="multipart/form-data" action=""> 
        <h4>Mensajes por defecto globales</h4><br />
        <div class="row"> 
            
            <div class="col-md-6">
                
                <div id="field-respuesta" class="form-group">
                    <label for="respuesta_correcta">Respuesta correcta *</label>
                    <textarea class="form-control" id="respuesta_correcta" name="respuesta_correcta" rows="2" maxlength="280"><?php if(!empty($mensajes['respuesta_correcta'])): echo $mensajes['respuesta_correcta'];  endif; ?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div id="field-respuesta" class="form-group">
                    <label for="respuesta_incorrecta">Respuesta incorrecta *</label>
                    <textarea class="form-control" id="respuesta_incorrecta" name="respuesta_incorrecta" rows="2" maxlength="280"><?php if(!empty($mensajes['respuesta_incorrecta'])): echo $mensajes['respuesta_incorrecta'];  endif; ?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div id="field-respuesta" class="form-group">
                    <label for="ganar">Mensaje al ganar *</label>
                    <textarea class="form-control" id="ganar" name="ganar" rows="2" maxlength="280"><?php if(!empty($mensajes['ganar'])): echo $mensajes['ganar'];  endif; ?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div id="field-respuesta" class="form-group">
                    <label for="perder">Mensaje al perder *</label>
                    <textarea class="form-control" id="perder" name="perder" rows="2" maxlength="280"><?php if(!empty($mensajes['perder'])): echo $mensajes['perder'];  endif; ?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div id="field-respuesta" class="form-group">
                    <label for="gps_cerca">Comodín GPS - Estás cerca *</label>
                    <textarea class="form-control" id="gps_cerca" name="gps_cerca" rows="2" maxlength="280"><?php if(!empty($mensajes['gps_cerca'])): echo $mensajes['gps_cerca'];  endif; ?></textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div id="field-respuesta" class="form-group">
                    <label for="gps_lejos">Comodín GPS - Estás lejos *</label>
                    <textarea class="form-control" id="gps_lejos" name="gps_lejos" rows="2" maxlength="280"><?php if(!empty($mensajes['gps_lejos'])): echo $mensajes['gps_lejos'];  endif; ?></textarea>
                </div>
            </div>
        </div>
<hr />
        <h4>Banner Patrocinador</h4><br />
        <div class="row">     
            <div class="col-md-6">
                <div id="field-patrocinador_imagen" class="form-group">
                    <label>Banner</label> 
                    <input type="file" id="patrocinador_imagen" name="patrocinador_imagen" class="dropify" data-default-file="<?php if(!empty($patrocinador_imagen))  echo site_url($patrocinador_imagen); ?>" data-allowed-file-extensions='["jpg", "jpeg", "png", "pneg", "gif", "bmp", "tif", "tiff"]' accept=".jpg,.jpeg,.png,.pneg,.gif,.bmp,.tif,.tiff" data-max-file-size="4M" />
                    <input type="hidden" id="patrocinador_imagen-editar" name="patrocinador_imagen-editar" value="0" />
                </div>
            </div>
            <div class="col-md-6">
                <div id="field-patrocinador_enlace" class="form-group">
                    <label for="texto">Enlace</label>
                    <input type="text" class="form-control" id="patrocinador_enlace" name="patrocinador_enlace" maxlength="280" value="<?php if(!empty($patrocinador_enlace))  echo $patrocinador_enlace; ?>"/>
                </div>
            </div>
        </div>
            
<hr />
        <h4>Reglas del juego</h4><br />
        <div class="row"> 
            <div class="col-md-12">
                <input type="hidden" id="reglas" name="reglas" value="" />
                <div id="reglas-editor" name="reglas" style="height: 400px;">
                    <?php echo $reglas; ?>
                </div> <!-- end Snow-editor-->
            </div>
        </div>
        <hr />
        <div id="msj" class="card-title"></div>
        
        <button id="boton-guardar" type="button" class="btn btn-primary mb-2">Guardar</button>
    </form>
    
</div>


<script>
jQuery(function($) {
    $(document).ready(function(){
        var quill = new Quill('#reglas-editor', {
            theme: 'snow',
            modules: {
                'toolbar': [ ['bold', 'italic', 'underline', 'strike'], [{ 'color': [] }, { 'background': [] }], [{ 'script': 'super' }, { 'script': 'sub' }], [{ 'header': [false, 1, 2, 3, 4, 5, 6] }, 'blockquote', 'code-block'], [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }], ['direction', { 'align': [] }], ['link'], ['clean']]
            },
        });

    });
    
    $('#boton-guardar').click(function(e){
        $('#msj').html('');
        
        var myEditor = document.querySelector('#reglas-editor')
        var html = myEditor.children[0].innerHTML

        $('#reglas').val(html);

        if($('#reglas').val()==''||$('#respuesta_correcta').val()==''||$('#respuesta_incorrecta').val()==''||$('#ganar').val()==''||$('#perder').val()==''||$('#gps_cerca').val()==''||$('#gps_lejos').val()==''){
            $('#msj').html('Debes rellenar los campos obligatorios.');
            return false;
        }

        if($('#msj').html()!=''){ 
            e.preventDefault();
            return false;
        } 
        $('#form-datos').submit();
        return true;
    });

    $("#field-patrocinador_imagen input[type=file]").change(function(){
        $('#patrocinador_imagen-editar').val(1);
    });
    $('#field-patrocinador_imagen .dropify-clear').click(function(){
       $('#patrocinador_imagen-editar').val(1);
    });

    
});

</script>