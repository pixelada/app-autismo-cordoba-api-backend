<h1 class="display-5">Nueva cita</h1>
<hr />
<a href="<?php echo site_url('admin/events'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    <i class="feather-arrow-left"></i> Volver al listado
</a>
<hr />
<div id="datos-item">
    <form id="form-item" method="post" enctype="multipart/form-data" action=""> 
        <div class="row"> 
            <div class="col-md-6">    
                <div id="field-date" class="form-group">
                    <label>Fecha y hora *</label>
                    <div id="datetime" class="datetimepicker"></div>
                    <input type="hidden" id="datetime_hidden" class="datetimepicker_input_hidden" name="datetime"/>
                </div>   
                <div class="form-group" id="field-user" style="paddng-left:20px;">
                    <label for="user">Usuario *</label><br>
                    <select class="form-control required" id="user" name="user_id">
                        <option value=""></option>
                        <?php 
                        $users = $this->user_model->users();
                        foreach($users as $user): 
                            echo '<option value="'.$user['id'].'">'.$user['name'].'</option>';
                            endforeach;    
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">  
                <div id="field-title" class="form-group">
                    <label for="simpleinput">Título *</label>
                    <input type="text" id="title" name="title" class="form-control required" value="" placeholder=""/>
                </div>
                <div id="field-description" class="form-group">
                    <label for="description">Descripción</label>
                    <textarea class="form-control" id="description" name="description" rows="2" maxlength="280"></textarea>
                </div>
            </div>
        </div>
        <hr />
        <p>* Campos obligatorios</p>
        <div id="msj" class="card-title"></div>
        
        <button id="button-save" type="button" class="btn btn-primary mb-2">Crear cita</button>
    </form>
</div>


<script>
jQuery(function($) {
    $('#button-save').click(function(e){
        $('#msj').html('');

        $('#form-item .form-group .required').each(function(){
            if($(this).val()==''){
                $('#msj').html('Debes rellenar los campos obligatorios');
                return false;
            }
        });

        if($('#msj').html()!=''){ 
            e.preventDefault();
            return false;
        } 
        $('#form-item').submit();
        return true;
    });
    
    $('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',inline: true,sideBySide: true,
    });
    $('.datetimepicker').each(function(){
        var date = format_date($(this).data("DateTimePicker").viewDate()._d);  
        $(this).parent().find('.datetimepicker_input_hidden').val(date);
        $(this).on("dp.change", function (e) {
            var date = format_date($(this).data("DateTimePicker").viewDate()._d); 
            $(this).parent().find('.datetimepicker_input_hidden').val(date);
        });
    }); 
    
});

    
</script>

                                