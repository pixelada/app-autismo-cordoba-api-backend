<h1 class="display-5">Editar cita</h1>
<hr />
<a href="<?php echo site_url('admin/events'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    <i class="feather-arrow-left"></i> Volver al listado
</a>
<hr />

<div id="datos-item">
    <form id="form-item" method="post" enctype="multipart/form-data" action=""> 
        <div class="row"> 
            <div class="col-md-6">    
                <div id="field-date" class="form-group">
                    <label>Fecha y hora *</label>
                    <div id="datetime" class="datetimepicker"></div>
                    <input type="hidden" id="datetime_hidden" class="datetimepicker_input_hidden" name="datetime" value="<?php if(!empty($item->datetime)) echo $item->datetime; ?>"/>
                </div>   
                <div class="form-group" id="field-user" style="paddng-left:20px;">
                    <label for="user">Usuario *</label><br>
                    <select class="form-control required" id="user" name="user_id">
                        <option value=""></option>
                        <?php 
                        $users = $this->user_model->users();
                        foreach($users as $user): 
                            echo '<option value="'.$user['id'].'" ';
                            if($user['id']==$item->user_id) echo 'selected';
                            echo '>'.$user['name'].'</option>';
                            endforeach;    
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">  
                <div id="field-description" class="form-group">
                    <label for="description">Descripción *</label>
                    <textarea class="form-control required" id="description" name="description" rows="2" maxlength="280"><?php if(!empty($item->description)) echo $item->description; ?></textarea>
                </div>
            </div>
        </div>
        <hr />
       
        <h3>Secuencias</h3>
        <input type="hidden" name="sequences" id="sequences" value="" /><br />

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modal-add">Añadir secuencia</button><br /><br />

        <div class="modal fade modal-add" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title h4" id="myExtraLargeModalLabel">Añadir secuencia</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <?php $sequences = $this->sequence_model->sequences(); 
                        foreach($sequences as $sequence): $pictogram = $this->pictogram_model->pictogram($sequence['thumbnail_id']);  ?>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body text-center">
                                    <img src="<?php echo site_url($pictogram->image); ?>" width="150" /><br />
                                    <input type="hidden" value="<?php echo $sequence['id']; ?>"
                                    <h4 class="font-weight-bold mt-4 text-uppercase"><?php echo $sequence['description'];  ?></h4> <br />
                                    
                                    <a class="btn btn-primary btn-add-sequence mt-4 mb-2 btn-rounded"><i class="fas fa-plus"></i></a>
                                </div>
                            </div> <!-- end Pricing_card -->
                        </div> <!-- end col -->
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="sortable" class="row">
    <?php foreach($item->sequences as $sequence): $sequence = (array) $sequence; $pictogram = $this->pictogram_model->pictogram($sequence['thumbnail_id']); ?>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <img src="<?php echo site_url($pictogram->image); ?>" width="150" /><br />
                    <input type="hidden" value="<?php echo $sequence['id']; ?>"
                    <h4 class="font-weight-bold mt-4 text-uppercase"><?php echo $sequence['description'];  ?></h4> <br />
                    
                    <a class="btn btn-primary mt-4 mb-2 btn-rounded"><i class="fas fa-times"></i></a>
                </div>
            </div> <!-- end Pricing_card -->
        </div> <!-- end col -->
    <?php endforeach; ?>

    </div>
        <hr />

        <p>* Campos obligatorios</p>
        <div id="msj" class="card-title"></div>
        
        <button id="button-save" type="button" class="btn btn-primary mb-2">Guardar cita</button>
    </form>

    <br /><br />
</div>


<script>
jQuery(function($) {
    resort();

    $('.btn-add-sequence').click(function(){
        var html = $(this).parent().parent().html();
        $('#sortable').append('<div class="col-md-3"><div class="card">'+html+'</div></div>');
        $('#sortable .btn-add-sequence i.fas').removeClass('fa-plus');
        $('#sortable .btn-add-sequence i.fas').addClass('fa-times');
        $('#sortable .btn-add-sequence').removeClass('btn-add-sequence');
        setTimeout(function(){
            $( "#sortable a.btn" ).click(function(){
                $(this).parent().parent().parent().remove();
                resort();
            });
        }, 1000);
        resort();
        
    });

    $( "#sortable a.btn" ).click(function(){
        $(this).parent().parent().parent().remove();
        resort();
    });

    $( "#sortable" ).sortable({update: function( ) {
        resort();
    }});

    $('#button-save').click(function(e){
        $('#msj').html('');
        resort();

        $('#form-item .form-group .required').each(function(){
            if($(this).val()==''){
                $('#msj').html('Debes rellenar los campos obligatorios');
                return false;
            }
        });

        if($('#msj').html()!=''){ 
            e.preventDefault();
            return false;
        } 
        $('#form-item').submit();
        return true;
    });
    
    $('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',inline: true,sideBySide: true,
        <?php if(!empty($item->datetime)): echo 'date: "'.$item->datetime.'"'; endif; ?>
    });
    $('.datetimepicker').each(function(){
        var date = format_date($(this).data("DateTimePicker").viewDate()._d);  
        $(this).parent().find('.datetimepicker_input_hidden').val(date);
        $(this).on("dp.change", function (e) {
            var date = format_date($(this).data("DateTimePicker").viewDate()._d); 
            $(this).parent().find('.datetimepicker_input_hidden').val(date);
        });
    }); 
    
});

function resort(){
    var sort = '';
    $("#sortable input").each(function(){
        if(sort!='') sort = sort + ',';
        sort = sort + $(this).val();
    });
    $('#sequences').val(sort);
}


    
</script>

<script>
 <?php $i=0; if($i>0): ?>
    $('#tabla-sequences').DataTable({
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        },
        "pageLength" : 25,
        "drawCallback": function () {
            $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
    });
<?php endif; ?>
    $('#filter-user').change(function(){
        $('#filter-list').submit();
    });
</script>