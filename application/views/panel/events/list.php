<h1 class="display-5">Citas</h1>
<hr />
<a id="boton-nuevo" href="<?php echo site_url('admin/events/new'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    Nueva cita
</a>

<div id="lista-events" class="row">
<div class="col-md-6">

<form id="filter-list" method="get">
<label for="filter-user">Filtrar usuario</label> 
<select id="filter-user" name="user">
    <option value="">Cualquiera</option> 
    <?php  $users = $this->user_model->users();
        foreach($users as $user):
            echo '<option value="'.$user['id'].'"';
            if(!empty($_GET['user'])&&$_GET['user']===$user['id']) echo 'selected';
            echo '>'.$user['id'].' - '.$user['name'].'</option>';
        endforeach;    
    ?>
    </select> &nbsp; 
    <label for="filter-time">Filtrar citas</label> 
<select id="filter-time" name="time">
    <option value="">Todas las citas</option> 
    <option value="future" <?php if(!empty($_GET['time'])&&$_GET['time']=='future') echo 'selected'; ?>>Solo citas futuras</option>
    <option value="past" <?php if(!empty($_GET['time'])&&$_GET['time']=='past') echo 'selected'; ?>>Solo citas pasadas</option>
    </select>
</form>


    <table id="tabla-events" class="table dt-responsive nowrap">
        <thead>
            <tr>
                <th>ID</th>
                <th>Descripción</th>
                <th>Fecha</th>
                <th>Usuario</th>
                <th width="30"></th>
            </tr>
        </thead>
    
    
        <tbody>
        <?php $i=0; foreach($events as $event): $event['user'] = $this->user_model->user_by_id($event['user_id']);  
        if(empty($_GET['user'])||($_GET['user']===$event['user_id'])): 
        if(empty($_GET['time'])||($_GET['time']=='future'&&strtotime($event['datetime'])>=time())||($_GET['time']=='past'&&strtotime($event['datetime'])<time())):
            $i++; 
        ?>
            <tr>
                <td><?php echo $event['id']; ?></td>
                <td><a href="<?php echo site_url('admin/events/edit/'.$event['id']); ?>"><strong><?php echo $event['description']; ?></strong></a></td>
                <td><?php echo $event['datetime']; ?></td>
                <td><?php if(!empty($event['user']['name'])):?> <a href="<?php echo site_url('admin/users/edit/'.$event['user']['id']); ?>"><?php echo $event['user']['name']; endif;  ?></td>
                <td>
                    <a class="link-editar" href="<?php echo site_url('admin/events/edit/'.$event['id']); ?>"><i class="fas fa-edit"></i></a>
                    <a class="link-eliminar" onclick="return confirm('¿Deseas eliminar esta cita?')" href="<?php echo site_url('admin/events/delete/'.$event['id']); ?>"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>
            <?php endif; endif; endforeach; ?>
        <?php if($i==0) echo '<tr><td colspan="6">No se encontraron resultados</td></tr>'; ?>
        </tbody>
    </table>
</div>
</div>

<script>
<?php if($i>0): ?>
    $('#tabla-events').DataTable({
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        },
        "order" : [[1, 'desc']],
        "pageLength" : 25,
        "scrollX": true,
        "autoWidth": false,
        "drawCallback": function () {
            $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
    });
<?php endif; ?>
    $('#filter-user').change(function(){
        $('#filter-list').submit();
    });

    $('#filter-time').change(function(){
        $('#filter-list').submit();
    });
</script>