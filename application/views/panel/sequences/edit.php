<h1 class="display-5">Editar secuencia</h1>
<hr />
<a href="<?php echo site_url('admin/sequences'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    <i class="feather-arrow-left"></i> Volver al listado
</a>
<hr />
<div id="datos-item">
    <form id="form-item" method="post" enctype="multipart/form-data" action=""> 
        <div class="row"> 
            <div class="col-md-6">    
                <div id="field-title" class="form-group">
                    <label for="simpleinput">Título *</label>
                    <input type="text" id="title" name="title" class="form-control required" value="<?php if(!empty($item->title)) echo $item->title; ?>" placeholder=""/>
                </div>
                <div id="field-description" class="form-group">
                    <label for="description">Descripción</label>
                    <textarea class="form-control" id="description" name="description" rows="2" maxlength="280"><?php if(!empty($item->description)) echo $item->description; ?></textarea>
                </div>  
                <div class="form-group" id="field-user" style="paddng-left:20px;">
                    <label for="user">Usuario</label><br>
                    <select class="form-control" id="user" name="author_id">
                        <option value="">Para todos los usuarios</option>
                        <?php 
                        $users = $this->user_model->users();
                        foreach($users as $user): 
                            echo '<option value="'.$user['id'].'" ';
                            if($user['id']==$item->author_id) echo 'selected';
                            echo '>'.$user['name'].'</option>';
                            endforeach;    
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">  
               
            </div>
        </div>
        <hr />
        <h3>Pictogramas</h3>
        <input type="hidden" name="pictograms" id="pictograms" value="" /><br />
    
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modal-add">Añadir pictograma</button><br /><br />

        <div class="modal fade modal-add" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title h4" id="myExtraLargeModalLabel">Añadir pictograma</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                        <?php $pictograms = $this->pictogram_model->pictograms();
                        foreach($pictograms as $pictogram): ?>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body text-center">
                                    <img src="<?php echo site_url($pictogram['image']); ?>" width="150" /><br />
                                    <input type="hidden" value="<?php echo $pictogram['id']; ?>"
                                    <h4 class="font-weight-bold mt-4 text-uppercase"><?php echo $pictogram['description'];  ?></h4> <br />
                                    
                                    <a class="btn btn-primary btn-add-pictogram mt-4 mb-2 btn-rounded"><i class="fas fa-plus"></i></a>
                                </div>
                            </div> <!-- end Pricing_card -->
                        </div> <!-- end col -->
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div id="sortable" class="row">
    <?php foreach($item->pictograms as $pictogram): ?>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body text-center">
                    <img src="<?php echo site_url($pictogram['image']); ?>" width="150" /><br />
                    <input type="hidden" value="<?php echo $pictogram['id']; ?>"
                    <h4 class="font-weight-bold mt-4 text-uppercase"><?php echo $pictogram['description'];  ?></h4> <br />
                    
                    <a class="btn btn-primary mt-4 mb-2 btn-rounded"><i class="fas fa-times"></i></a>
                </div>
            </div> <!-- end Pricing_card -->
        </div> <!-- end col -->
    <?php endforeach; ?>

    </div>
        <hr />
        <p>* Campos obligatorios</p>
        <div id="msj" class="card-title"></div>
        
        <button id="button-save" type="button" class="btn btn-primary mb-2">Guardar secuencia</button>

    </form>

</div>

<script>

jQuery(function($) {
    resort();

    $('.btn-add-pictogram').click(function(){
        var html = $(this).parent().parent().html();
        $('#sortable').append('<div class="col-md-3"><div class="card">'+html+'</div></div>');
        $('#sortable .btn-add-pictogram i.fas').removeClass('fa-plus');
        $('#sortable .btn-add-pictogram i.fas').addClass('fa-times');
        $('#sortable .btn-add-pictogram').removeClass('btn-add-pictogram');
        setTimeout(function(){
            $( "#sortable a.btn" ).click(function(){
                $(this).parent().parent().parent().remove();
                resort();
            });
        }, 1000);
        resort();
    });

    $( "#sortable a.btn" ).click(function(){
        $(this).parent().parent().parent().remove();
        resort();
    });

    $( "#sortable" ).sortable({update: function( ) {
        resort();
    }});
    $( "#sortable" ).disableSelection();

    $('#button-save').click(function(e){
        
        resort();


        $('#msj').html('');
        if($('#field-imagen .dropify-preview .dropify-render').html()==''){
            $('#msj').html('Debes rellenar los campos obligatorios');
            return false;
        }

        $('#form-item .form-group .required').each(function(){
            if($(this).val()==''){
                $('#msj').html('Debes rellenar los campos obligatorios');
                return false;
            }
        });

        if($('#msj').html()!=''){ 
            e.preventDefault();
            return false;
        } 
        $('#form-item').submit();
        return true;
    });
    

    
});

function resort(){
    var sort = '';
    $("#sortable input").each(function(){
        if(sort!='') sort = sort + ',';
        sort = sort + $(this).val();
    });
    $('#pictograms').val(sort);
}

</script>

                                