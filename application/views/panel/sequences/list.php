<h1 class="display-5">Secuencias</h1>
<hr />
<a id="boton-nuevo" href="<?php echo site_url('admin/sequences/new'); ?>" style="color:#fff;" class="btn btn-primary d-none d-lg-block ml-2">
    Nueva secuencia
</a>

<div id="lista-sequences" class="row">
<div class="col-md-6">

<form id="filter-list" method="get">
<label for="filter-user">Filtrar autor</label> 
<select id="filter-user" name="user">
    <option value="">Todas las secuencias</option> 
    <option value="0" <?php if(isset($_GET['user'])&&$_GET['user']=="0"): echo 'selected'; endif;  ?>>Secuencias generales</option> 
    <?php  $users = $this->user_model->users();
        foreach($users as $user):
            echo '<option value="'.$user['id'].'"';
            if(!empty($_GET['user'])&&$_GET['user']===$user['id']) echo 'selected';
            echo '>'.$user['id'].' - '.$user['name'].'</option>';
        endforeach;    
    ?>
    </select>
</form>
<?php //echo $this->option_model->get_token(); ?>

    <table id="tabla-sequences" class="table dt-responsive nowrap">
        <thead>
            <tr>
                <th>ID</th>
                <th>Descripción</th>
                <th>Miniatura</th>
                <th>Usuario</th>
                <th width="30"></th>
            </tr>
        </thead>
    
    
        <tbody>
        <?php $i=0; foreach($sequences as $sequence): $sequence['user'] = $this->user_model->user_by_id($sequence['author_id']);  $thumbnail = $this->sequence_model->sequence_thumbnail($sequence['id']);
        if(!isset($_GET['user'])||(isset($_GET['user'])&&$_GET['user']=="0"&&empty($sequence['author_id']))||(isset($_GET['user'])&&$_GET['user']!='0'&&empty($_GET['user']))||(!empty($_GET['user'])&&$_GET['user']===$sequence['author_id'])): $i++; ?>
            <tr>
                <td><?php echo $sequence['id']; ?></td>
                <td><a href="<?php echo site_url('admin/sequences/edit/'.$sequence['id']); ?>"><strong><?php echo $sequence['title']; ?></strong></a></td>
                <td><?php if(!empty($thumbnail)): ?><a href="<?php echo site_url('admin/sequences/edit/'.$sequence['id']); ?>"><img height="100" src="<?php echo $thumbnail; ?>" alt="<?php echo $sequence['title']; ?>" /></a><?php endif; ?></td>
                <td><?php if(!empty($sequence['user']['name'])):?> <a href="<?php echo site_url('admin/users/edit/'.$sequence['user']['id']); ?>"><?php echo $sequence['user']['name']; endif;  ?></td>
                <td>
                    <a class="link-editar" href="<?php echo site_url('admin/sequences/edit/'.$sequence['id']); ?>"><i class="fas fa-edit"></i></a>
                    <a class="link-eliminar" onclick="return confirm('¿Deseas eliminar esta secuencia?')" href="<?php echo site_url('admin/sequences/delete/'.$sequence['id']); ?>"><i class="fas fa-trash-alt"></i></a>
                </td>
            </tr>
        <?php endif; endforeach; ?>
        <?php if($i==0) echo '<tr><td colspan="5">No se encontraron resultados</td></tr>'; ?>
        </tbody>
    </table>
</div>
</div>

<script>
 <?php if($i>0): ?>
    $('#tabla-sequences').DataTable({
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        },
        "scrollX": true,
        "autoWidth": false,
        "pageLength" : 25,
        "drawCallback": function () {
            $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
    });
<?php endif; ?>
    $('#filter-user').change(function(){
        $('#filter-list').submit();
    });
</script>